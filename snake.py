import pygame
from enum import Enum
from random import randint

# Nastavení globálních souřadnic
GRID_SIZE = 40
WIDTH = 1600
HEIGHT = 1600

# Enumerátor pro rozlišování pohybu hada
class Direction(Enum):
    UP = 1
    DOWN = 2
    LEFT = 3
    RIGHT = 4

# Třída Snake
class Snake():
    # Nastavení výchozí pozice hada při vytvoření instance třídy Snake
    def __init__(self):
        self.snake = []
        self.set_default_snake_position()

    # Funkce vrátí list hada
    def get_snake(self):
        return self.snake
    
    # Funkce pro získání prvního elementu hada
    def get_snake_head(self):
        return self.snake[0]
    
    # Funkce pro nastavení výchozí pozice hada
    def set_default_snake_position(self):
        # Připadné vymazání všech předchozích elementů  listu snake
        self.get_snake().clear()

        # Vytvoření vektoru s defaultních pozici hada na začátku hry
        head_pos = pygame.Vector2(WIDTH / 2, HEIGHT / 2)
        body_pos = pygame.Vector2(WIDTH / 2, HEIGHT / 2 + GRID_SIZE)
        tail_pos = pygame.Vector2(WIDTH / 2, HEIGHT / 2 + 2 * GRID_SIZE)

        # Přidání instancí třídy pygame.Rect do listu snake
        self.snake.append(pygame.Rect(head_pos.x, head_pos.y, GRID_SIZE, GRID_SIZE))
        self.snake.append(pygame.Rect(body_pos.x, body_pos.y, GRID_SIZE, GRID_SIZE))
        self.snake.append(pygame.Rect(tail_pos.x, tail_pos.y, GRID_SIZE, GRID_SIZE))

    # Funkce pro vykreslení všech částí hada
    def render_snake(self, screen):
        for block in self.snake:
            # Druhý argument je tuple RGBA hodnota tmavě zelené barvy (40, 105, 15, 1)
            pygame.draw.rect(screen, (40, 105, 15, 1), block)

    # Simulace pohybu hada
    def move(self, current_direction):
        # Iterování listem hada od zadu, nastaví pozici elementu z předchozí pozice
        for i in range(len(self.snake) - 1, 0, -1):
            self.snake[i].left = self.snake[i - 1].left
            self.snake[i].top = self.snake[i - 1].top

        # Posunutí prvního elementu hada o konstantu GRID_SIZE ve směru současného pohybu
        if current_direction is Direction.UP:
            self.get_snake_head().top -= GRID_SIZE
        if current_direction is Direction.DOWN:
            self.get_snake_head().top += GRID_SIZE
        if current_direction is Direction.LEFT:
            self.get_snake_head().left -= GRID_SIZE
        if current_direction is Direction.RIGHT:
            self.get_snake_head().left += GRID_SIZE

class Game():
    # Zavolání metody init_variables v kontruktoru třídy
    def __init__(self):
        self.init_variables()

    # Nastavení defaultních proměnných
    def init_variables(self):
        self.apple = self.init_apple()
        self.game_speed = 100
        self.current_direction = Direction.UP
        self.time_counter = 0
        self.score = 0
        self.game_over = False

    def init_apple(self):
        # Náhodné vygenerovnání nových x a y souřadnice
        apple_pos = self.generate_random_apple_pos()

        # Vytvoření instance třídy pygame.Rect(), s vygenerovnými souřadnicemi a velikostí mřížky
        apple = pygame.Rect(apple_pos[0], apple_pos[1], GRID_SIZE, GRID_SIZE)

        return apple

    # Náhodné vygenerovnání nových x a y souřadnice pro pozici jablka
    def generate_random_apple_pos(self):
        return pygame.Vector2(randint(0, WIDTH / GRID_SIZE - 1) * GRID_SIZE, randint(0, HEIGHT / GRID_SIZE - 1) * GRID_SIZE)

    def regenerate_apple(self):
        # Náhodné vygenerovnání nových x a y souřadnice
        apple_pos = self.generate_random_apple_pos()
        # Nastavení vygenerovaných souřadnic na obdelník jablka
        self.apple.left = apple_pos[0]
        self.apple.top = apple_pos[1]

    # Kontroluje zda se had nachází mimo hranice hry
    def out_of_boundaries(self, snake):
        # Definování proměnné hlavy hada
        snake_head = snake.get_snake_head()
        # Kontrolo zda se had nachází nalevo od hranic obrazovky
        # Nebo napravo od hranic obrazovky, pokud ano hra se ukončí
        if snake_head.left < 0 or snake_head.right > WIDTH:
            self.game_over = True
        # Kontrolo zda se had nachází nahoru od hranic obrazovky
        # Nebo vespod od hranic obrazovky, pokud ano hra se ukončí
        elif snake_head.top < 0 or snake_head.bottom > HEIGHT:
            self.game_over = True

    # Funkce která kontroluje kolizi hada sama se sebou
    def self_colision(self, snake):
        # Definování souřadnic hlavy hada
        head_top = snake.get_snake_head().top
        head_left = snake.get_snake_head().left
        
        # Iterování všech části hada kromě hlavy, tedy prvního elementu v listu
        for body in snake.get_snake()[1:]:
            # Definování souřadnic těla hada
            body_top = body.top
            body_left = body.left
            # Pokud se souřadnice hlavy hada shoduje s nějakou souřadnicí těla
            # Nastaví proměnou game_over jako True a ukoční tak hru
            if head_top == body_top and head_left == body_left:
                self.game_over = True

    # Naslouchání vstupu z klávesnice pro změnu směru hada
    # Podmínka zakazuje změnu v opačném směru
    def get_direction_input(self, current_direction):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP] and current_direction is not Direction.DOWN:
            current_direction = Direction.UP
        elif keys[pygame.K_DOWN] and current_direction is not Direction.UP:
            current_direction = Direction.DOWN
        elif keys[pygame.K_LEFT] and current_direction is not Direction.RIGHT:
            current_direction = Direction.LEFT
        elif keys[pygame.K_RIGHT] and current_direction is not Direction.LEFT:
            current_direction = Direction.RIGHT

        return current_direction
    
    def check_apple_collision(self, snake):
        # Definování proměnných
        snake_list = snake.get_snake()
        snake_last_index = len(snake.get_snake()) - 1
        snake_head = snake.get_snake_head()
        # Pokud souřadnice hlavy hada se shodují se souřadnicemi jablka
        if snake_head.left == self.apple.left and snake_head.top == self.apple.top:
            # Jablko vygeneruje nové souřadnice
            self.regenerate_apple()

            # Přidání obdelníku do listu snake na souřadnicích posledního článku
            added_body_pos = pygame.Vector2(snake_list[snake_last_index].left, snake_list[snake_last_index].right)
            snake_list.append(pygame.Rect(added_body_pos.x, added_body_pos.y, 40, 40))
            # Zrychlení hry a přidání skóre
            self.game_speed -= 2
            self.score += 1

    # Funkce která zarovná text na určité dimenzi a případně posune o daný offset
    def center_text_width(self, image, offset=0):
        return int(WIDTH / 2 - image.get_width() / 2 + offset)

    # Funkce která zarovná text na určité dimenzi a případně posune o daný offset
    def center_text_height(self, image, offset=0):
        return int(HEIGHT / 2 - image.get_height() / 2 + offset)

    def game_loop(self):
        # Nastavení herních proměných
        clock = pygame.time.Clock()
        running = True

        # Nastavení fontů pro zobrazování textu
        scoreFont = pygame.font.SysFont(None, 48)
        gameoverFont = pygame.font.SysFont(None, 160)
        restartGameFont = pygame.font.SysFont(None, 80)
        endGameFont = pygame.font.SysFont(None, 60)

        # Vytvoření instance třídy Snake
        snake = Snake()

        # Herní smyčka která běží zatím co je proměnná running True
        while running:
            # Přičtení času k časomíře, která je nezávislá na FPS
            self.time_counter += clock.tick()
            # Naslouchání události zavření okna
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            # Nastavení barvy pozadí
            screen.fill("green")

            # Pokud hráč neprohrál, tedy pokud hra běží
            if not self.game_over:
                # Pokud časomíra překročí stanovenou rychlost hry
                # Spustí se následující metody
                if self.time_counter > self.game_speed:
                    # Naslouchání vstupu od uživatele a případná změna směrů
                    self.current_direction = self.get_direction_input(self.current_direction)

                    # Zavolání metody move, která zajištuje pohyb hada
                    snake.move(self.current_direction)

                    # Kontrolování zda had se nachází mimo obrazovku
                    self.out_of_boundaries(snake)

                    # Kontrolování zda se had srazí sám se sebou
                    self.self_colision(snake)

                    # Kontrolování zda hráč narazil na jablko
                    self.check_apple_collision(snake)

                    # Vyresetování časomíry pro další krok
                    self.time_counter = 0

            # Zobrazení textů pokud hráč prohrál
            else:
                # Vypočítej pozici a image, krerý vykreslí GAME OVER text
                gameoverImg = gameoverFont.render('GAME OVER', True, 'red')
                screen.blit(gameoverImg, (self.center_text_width(gameoverImg), self.center_text_height(gameoverImg)))

                # Vypočítej pozici a image, který vykreslí "restart game" text
                restartGameImg = restartGameFont.render('Press R to restart game', True, 'black')
                screen.blit(restartGameImg, (self.center_text_width(restartGameImg), self.center_text_height(restartGameImg, 100)))

                # Vypočítej pozici a image, který vykreslí "end game" text
                endGameImg = endGameFont.render('Press E to end game', True, 'black')
                screen.blit(endGameImg, (self.center_text_width(endGameImg), self.center_text_height(endGameImg, 160)))

                # Naslouchání události ukončení hry klávesou "e"
                keys = pygame.key.get_pressed()
                if keys[pygame.K_e]:
                    running = False
                
                # Naslouchání události resetování hry klávesou "r"
                elif keys[pygame.K_r]:
                    self.init_variables()
                    snake.set_default_snake_position()

            # Vykresli hada
            snake.render_snake(screen)

            # Vykresli jablko
            pygame.draw.rect(screen, (166, 0, 0, 1), self.apple)

            # Vykresli text se skórem
            img = scoreFont.render('Score: ' + str(self.score), True, 'black')
            screen.blit(img, (WIDTH - 170, 20))

            # Updatování obrazovky, která vykreslí třídu Surface na obrazovku
            pygame.display.flip()

if __name__ == "__main__":
    # Inicializování knihovny pygame
    pygame.init()

    # Nastavení šířky a výšky obrazocky
    screen = pygame.display.set_mode((WIDTH, HEIGHT));

    # Nastavení jména okna
    pygame.display.set_caption("Snake")

    # Vytvoření instance třídy Game
    game = Game()

    # Spuštění herní smyčky
    game.game_loop()

    # Ukočení knihovny pygame
    pygame.quit()
